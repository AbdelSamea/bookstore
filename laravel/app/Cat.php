<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    protected $table='categories';
    protected $fillable=['name','updated_at','created_at'];
}
