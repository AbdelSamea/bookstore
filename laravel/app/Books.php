<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table='books';
    protected $fillable=['id','fdesc','download_link','auther','shdesc','cat_id','bestseller','launch_date','pages','image','name','updated_at','created_at'];}
