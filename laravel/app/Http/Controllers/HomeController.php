<?php

namespace App\Http\Controllers;

use App\Books;
use App\Cat;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $data;
    private $imagename;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view('home');
    }

    public function getAddCategory()
    {
        $cats=Cat::get();
        return view('addcat')->withcats($cats);
    }
    public function getDeleteCat($id)
    {
        $cat=Cat::find($id);
        $cat->delete();
        $cats=Cat::get();
        session::flash('msg', 'category deleted');
        return redirect('admin/add-category')->withcats($cats);
    }

    public function postAddCategory(Request $request)
    {
        $cat = new Cat;
        $cat->name = $request->input('cat');
        $cat->save();
        session::flash('msg', 'category ( ' . $cat->name . ' )  inserted');
        return redirect('admin/add-category');
    }

    public function getAddBook()
    {
        $cats = Cat::get();
        return view('addbook')->withcats($cats);
    }

    public function postAddBook(Request $request)
    {
        $dest='images/uploads/';
        $book = new Books;
        $book->name = $request->input('name');
        $book->fdesc = $request->input('fdesc');
        $book->auther = $request->input('auther');
        $book->bestseller = $request->input('bestseller');
        $book->launch_date = $request->input('launch_date');
        $book->cat_id = $request->input('cat_id');
        $book->shdesc = $request->input('shdesc');
        $book->pages = $request->input('pages');
        $book->download_link = $request->input('download_link');

        if ($request->hasFile('image')){
            $this->uploadimage($request,$dest);
            $book->image = $this->data['image'];
        }else
        {
            $book->image='0';
        }
        $book->save();
        session::flash('msg', 'book ( ' . $book->name . ' )  inserted');
        return redirect('admin/add-book');
    }

    public function getBooks()
    {
        $books = Books::get();
        return view('books')->withbooks($books);
    }
    public function getDeleteBook($id)
    {
        $book = Books::find($id);
        $name=$book->name;
        $book->delete();
        session::flash('msg', 'book ( ' . $name . ' )  deleted');
        return redirect('admin/books');
    }


    private function uploadimage(Request $request, $dest)
    {
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $this->imagename = rand(1,999).time() . $file->getClientOriginalName();
            $file->move($dest, $this->imagename);
            $this->data['image'] = $this->imagename;
        }
        return true;
    }

//    private function uploadimages(Request $request, $dest)
//    {
//
//        if ($request->hasFile('images')) {
//
//            $files = $request->file('images');
//            foreach ($files as $file) {
//                $this->imagename = time() . $file->getClientOriginalName();
//                $file->move($dest, $this->imagename);
//                $this->data['images'][] = $this->imagename;
//            }
//            return true;
//
//        }
//    }

    private
    function deleteimage($image, $dir)
    {
        $imagesfile = scandir($dir);
        foreach ($imagesfile as $img) {
            if ($image == "." || $image == "..")
                continue;

            if ($img == $image) {
                unlink($dir . $image);
            }
        }

    }

}
