<?php

namespace App\Http\Controllers;

use App\Books;
use App\Cat;
use Illuminate\Http\Request;

use App\Http\Requests;

class MainController extends Controller
{
    public function getIndex(){
        $books=Books::orderBy('id','desc')->get();
        $cats=Cat::get();
        $bseller=Books::where('bestseller',1)->orderBy('id','desc')->get();
        return view('index')->withbooks($books)->withcats($cats)->withbestseller($bseller);
    }
    public function getBook($id){
        $books=Books::orderBy('id','desc')->get();
        $book=Books::find($id);
        $cats=Cat::get();
        $bseller=Books::where('bestseller',1)->orderBy('id','desc')->get();
        return view('details')->withbookx($book)->withcats($cats)->withbestseller($bseller)->withbooks($books);
    }
    public function getAbout(){
        $books=Books::orderBy('id','desc')->get();
        $cats=Cat::get();
        $bseller=Books::where('bestseller',1)->orderBy('id','desc')->get();
        return view('about')->withcats($cats)->withbestseller($bseller)->withbooks($books);
    }


}
