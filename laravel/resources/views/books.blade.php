@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if(Session::has('msg'))
                            <div class="alert alert-success"> {{Session('msg')}}</div>
                        @endif

                        <table class="table table-hover text-center ">
                            <tr >
                                <th class="text-center">name</th>
                                <th class="text-center">auther</th>
                                <th class="text-center">delete</th>
                            </tr>
                            @foreach($books as $book)
                                <tr>
                                    <td>{{$book->name}}</td>
                                    <td>{{$book->auther}}</td>
                                    <td class="text-center"><a href="{{url('/admin/delete-book/'.$book->id)}}">X</a></td>
                                </tr>

                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
