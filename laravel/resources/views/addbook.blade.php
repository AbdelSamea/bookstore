@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if(Session::has('msg'))
                            <div class="alert alert-success"> {{Session('msg')}}</div>
                        @endif
                        <form action="{{url('/admin/add-book')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="name" placeholder="enter book name" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="shdesc" placeholder="enter short desc"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="download_link"
                                       placeholder="enter download link       EX:www.example.com  Note don't tybe http:// "
                                       class="form-control"></div>
                            <div class="form-group">
                                <input type="text" name="auther" placeholder="enter auther name"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="launch_date" placeholder="enter launch date"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="text" name="pages" placeholder="enter number of pages"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="image">image</label>
                                <input type="file" name="image" placeholder="enter number of pages"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <textarea name="fdesc" placeholder="enter full description "
                                          class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <select name="cat_id" class="form-control">
                                    @foreach($cats as $cat)
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="bestseller" class=""> Best seller:</label>
                                <br>
                                <label for="bestseller" class=""> yes</label>
                               <input type="radio" name="bestseller" value="1">
                                <br>
                                <label for="bestseller" class="">no &nbsp;</label>
                                <input type="radio" name="bestseller" value="0" checked    ="checked">
                            </div>
                            <input type="submit" name="submit" value="submit" class="btn btn-default">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
