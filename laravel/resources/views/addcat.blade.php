@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if(Session::has('msg'))
                      <div class="alert alert-success"> {{Session('msg')}}</div>
                    @endif
                    <form action="{{url('/admin/add-category')}}" method="post">
                        {{csrf_field()}}
                       <div class="form-group">
                           <input type="text" name="cat" placeholder="enter category name" class="form-control">
                       </div> <input type="submit" name="submit" value="submit" class="btn btn-default">
                    </form>
                </div>
                <div class="panel-heading">Categories</div>

                <div class="panel-body">
                    <table class="table table-hover text-center ">
                        <tr >
                            <th class="text-center">name</th>
                            <th class="text-center">delete</th>
                        </tr>
                        @foreach($cats as $cat)
                            <tr>
                                <td>{{$cat->name}}</td>
                                <td class="text-center"><a href="{{url('/admin/delete-cat/'.$cat->id)}}">X</a></td>
                            </tr>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
