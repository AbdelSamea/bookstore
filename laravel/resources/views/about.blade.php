<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon"
          type="image/png"
          href="{{url('images/logo.png')}}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>مقهى الكتب</title>
    <meta name="keywords"
          content="Book Store Template, Free CSS Template, CSS Website Layout, CSS, HTML, TemplateMo.com"/>
    <meta name="description"
          content="Book Store Template, Free CSS Template, Download CSS Website from TemplateMo.com"/>
    <link href="{{url('/templatemo_style.css')}}" rel="stylesheet" type="text/css"/>
</head>
<body>
<!--  Free CSS Templates from www.templatemo.com -->
<div id="templatemo_container">
    <div id="templatemo_menu">
        <ul>
            <li><a href="{{url('/index')}}" class="current">الرئيسية</a></li>
            <li><a href="{{url('/about')}}" class="current">معلومات عنا</a></li>

            {{--<li>--}}
            {{--<form action="">--}}
            {{--<input type="text" name="s" placeholder="search" style="margin-top: -5px;">--}}
            {{--</form>--}}
            {{--</li>--}}

        </ul>
    </div> <!-- end of menu -->

    <div id="templatemo_header">
        <div id="templatemo_special_offers">
            <ul>
                <?php $count = 1;?>
                @foreach($books as $book)

                    <li><a href="{{url('book/'.$book->id)}}">{{$book->name}}</a></li>

                    <?php $count++;
                    if ($count > 3)
                        break;
                    ?>
                @endforeach
            </ul>
        </div>


        <div id="templatemo_new_books">
            <ul>
                <?php $count = 1;?>
                @foreach($books as $book)
                    @if($count>3)
                        <li><a href="{{url('book/'.$book->id)}}">{{$book->name}}</a></li>
                    @endif
                    <?php $count++;
                    if ($count > 6)
                        break;
                    ?>
                @endforeach
            </ul>

        </div>
    </div> <!-- end of header -->

    <div id="templatemo_content">

        <div id="templatemo_content_left">
            <div class="templatemo_content_left_section">
                <h1>الفئات</h1>
                <ul>
                    <?php $count = 1;?>
                    @foreach($cats as $cat)

                        <li>{{$cat->name}}</li>
                        <?php $count++;
                        if ($count > 10)
                            break;
                        ?>
                    @endforeach
                </ul>
            </div>
            <div class="templatemo_content_left_section">
                <h1>الأكثر مبيعاً</h1>
                <ul>
                    <?php $count = 1;?>
                    @foreach($books as $book)

                        <li><a href="{{url('book/'.$book->id)}}">{{$book->name}}</a></li>
                        <?php $count++;
                        if ($count > 10)
                            break;
                        ?>
                    @endforeach
                </ul>
            </div>

        </div> <!-- end of content left -->

        <div id="templatemo_content_right">

            <h1>
                معلومات عنا
            </h1>


            <p style="font-size:16px; line-height: 28px;  font-family: Verdana">ايمانا منا باهمية القراءة في الارتقاء بالفكر والسلوك ودعما منا للقراءة وزيادة التشجيع عليها ولأن فى الكتب حيوات عدة لذا قدمنا هذا الموقع لمن لا يكتفى بحياة واحدة وعقل واحد وقلب واحد ومما لا يخفى على الجميع الصعوبات التى يلاقيها القراء فى العالم العربى بأكمله سواء من ارتفاع اسعار الكتب من جهة او عدم توافرها فى بعض المناطق من جهة اخرى لذا كان الكتاب الالكترونى هو الحل الأمثل للجميع ومع ذلك فلا يجوز ان ننشر الابداع دون رضى المبدعين لذا ايمانا منا بحق اى كاتب فى تقرير مصير ابداعه وحرصا على الملكية الفكرية فسوف نقوم بحذف اى كتاب يراسلنا كاتبه اذا كان لا يريد ان يستفيد من كتابه ملايين القراء وسوف نقوم بحذفه خلال 48 ساعة فقط</p>

            <h2><a href="https://www.facebook.com/groups/1596533417306058/?fref=ts">facebook group</a></h2>

            <div class="cleaner_with_height">&nbsp;</div>


        </div> <!-- end of content right -->

        <div class="cleaner_with_height">&nbsp;</div>
    </div> <!-- end of content -->

    <div id="templatemo_footer">
        <a href="{{url('/index')}}">Home</a>
        <br>
        Copyright © 2016
    </div> <!-- end of footer -->
    <!--  Free CSS Template www.templatemo.com -->
</div> <!-- end of container -->
</body>
</html>