<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon"
          type="image/png"
          href="{{url('images/logo.png')}}">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>مقهى الكتب</title>
    <meta name="keywords" content="Book Store Template, Free CSS Template, CSS Website Layout, CSS, HTML"/>
    <meta name="description" content="Book Store Template, Free CSS Template, Download CSS Website"/>
    <link href="{{url('/templatemo_style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css"/>
</head>
<body>
<!--  Free CSS Templates from www.templatemo.com -->
<div id="templatemo_container">
    <div id="templatemo_menu">
        <ul>
            <li><a href="{{url('/index')}}" class="current">الرئيسية</a></li>
            <li><a href="{{url('/about')}}" class="current">معلومات عنا</a></li>
            {{--<li>--}}
            {{--<form action="">--}}
            {{--<input type="text" name="s" placeholder="search" style="margin-top: -5px;">--}}
            {{--</form>--}}
            {{--</li>--}}
        </ul>
    </div> <!-- end of menu -->

    <div id="templatemo_header">
        <div id="templatemo_special_offers">
            <ul>
                <?php $count = 1;?>
                @foreach($books as $book)

                    <li><a href="{{url('book/'.$book->id)}}">{{substr($book->name,0,55)}}
                            @if(strlen($book->name)>55)...
                            @endif
                        </a></li>
                    <?php $count++;
                    if ($count > 3)
                        break;
                    ?>
                @endforeach
            </ul>
        </div>


        <div id="templatemo_new_books">
            <ul>
                <?php $count = 1;?>
                @foreach($books as $book)
                    @if($count>3)
                        <li><a href="{{url('book/'.$book->id)}}">{{substr($book->name,0,60)}}
                                @if(strlen($book->name)>55)...
                                @endif
                            </a></li>
                    @endif
                    <?php $count++;
                    if ($count > 6)
                        break;
                    ?>
                @endforeach
            </ul>
        </div>
    </div> <!-- end of header -->

    <div id="templatemo_content">

        <div id="templatemo_content_left">
            <div class="templatemo_content_left_section">
                <h1>الفئات</h1>
                <ul>
                    <?php $count = 1;?>
                    @foreach($cats as $cat)

                        <li>{{$cat->name}}</li>
                        <?php $count++;
                        if ($count > 10)
                            break;
                        ?>
                    @endforeach
                </ul>
            </div>
            <div class="templatemo_content_left_section">
                <h1>الأكثر مبيعاً</h1>
                <ul>
                    <?php $count = 1;?>
                    @foreach($books as $book)

                        <li><a href="{{url('book/'.$book->id)}}">{{$book->name}}</a></li>
                        <?php $count++;
                        if ($count > 10)
                            break;
                        ?>
                    @endforeach
                </ul>
            </div>

        </div> <!-- end of content left -->

        <div id="templatemo_content_right">
            @foreach($books as $book)
                <div class="templatemo_product_box">
                    <h1>{{substr($book->name,0,60)}} <span>(الكاتب:  {{substr($book->auther,0,25)}})</span></h1>
                    <img src="{{url('images/uploads/'.$book->image)}}" style="height:150px ;
                    width: 100px;" alt="image"/>
                    <div class="product_info">
                        <p>{{substr($book->shdesc,0,100)}}
                            @if(strlen($book->shdesc)>100)
                            ...
                            @endif
                        </p>
                        <div class="buy_now_button"><a href="http://{{$book->download_link}}">تحميل</a></div>
                        <div class="detail_button"><a href="{{url('book/'.$book->id)}}">تفاصيل</a></div>
                    </div>
                    <div class="cleaner">&nbsp;</div>
                </div>
            @endforeach


        </div> <!-- end of content right -->

        <div class="cleaner_with_height">&nbsp;</div>
    </div> <!-- end of content -->

    <div id="templatemo_footer">
        <a href="{{url('/index')}}">Home</a>
        <br>
        Copyright © 2016
    </div>

</div> <!-- end of container -->

</body>
</html>